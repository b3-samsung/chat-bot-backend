package com.example;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.StickerMessageContent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.StickerMessage;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.response.BotApiResponse;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;

@LineMessageHandler
public class LineBotLisener {

	private static final Logger log = LoggerFactory.getLogger(LineBotLisener.class);
	@Value("${api.baseurl}")
	private String apiBaseUrl;
	@Autowired
	private LineMessagingClient lineMessagingClient;
	
	@EventMapping
	public void handleTextMessage(MessageEvent<TextMessageContent> event) {
		log.info(apiBaseUrl);
		log.info(event.getSource().getSenderId());
		TextMessageContent message = event.getMessage();
		log.info(event.getMessage().getText());
		handleTextContent(event.getReplyToken(), event, message);
	}



	private void handleTextContent(String replyToken, Event event, TextMessageContent content) {
//		MyDatabase db = MyDatabase.getInstance();
		

		final String text = content.getText();
		String userId = event.getSource().getUserId();
//		if(userId != null) {
//			lineMessagingClient.getProfile(userId)
//			.whenComplete((profile, throwable) -> {
//				if(throwable != null) {
//					this.replyText(replyToken, throwable.getMessage());
//					return;
//				}
//				this.reply(replyToken, Arrays.asList(
//						new TextMessage("Display name: " + profile.getDisplayName()),
//						new TextMessage("Status message: " + profile.getStatusMessage()),
//						new TextMessage("User ID: " + profile.getUserId())		,
//						 new ReceiptFlexMessageSupplier().get()
//						));
//			});
//		}
		
		System.out.println( String.format("Got text message from %s : %s", replyToken, text));
		if ( "ข้อมูลของฉัน".equals(text) ) {
//			db.setState(event.getSource().getSenderId(), "ข้อมูลของฉัน");
			this.reply(replyToken, new ReceiptFlexMessageSupplier().get());
		}
		else if ("ทีมงานของฉัน".equals(text)){
//			db.setState(event.getSource().getSenderId(), "ทีมงานของฉัน");
             this.reply(replyToken, new CatalogueFlexMessageSupplier(apiBaseUrl).get() );
		}
		else {
//			db.setState(event.getSource().getSenderId(), "ทีมงานของฉัน");
//			System.out.println("Current State: " + db.getCurrentState(event.getSource().getSenderId()));
			System.out.println( String.format("Return echo message %s : %s", replyToken, text));
			this.replyText(replyToken, text);
		}
	}


	private void handleStickerContent(String replyToken, StickerMessageContent content) {
		reply(replyToken, new StickerMessage(
				content.getPackageId(), content.getStickerId()
				));
	}

	private void replyText(  String replyToken,  String message) {
		if(replyToken.isEmpty()) {
			throw new IllegalArgumentException("replyToken is not empty");
		}

		if(message.length() > 1000) {
			message = message.substring(0, 1000 - 2) + "...";
		}
		this.reply(replyToken, new TextMessage(message));
	}

	private void reply( String replyToken,  Message ...message) {
		reply(replyToken, Arrays.asList(message));
	}

	private void reply( String replyToken,  List<Message> messages) {
		try {
			BotApiResponse response = lineMessagingClient.replyMessage(
					new ReplyMessage(replyToken, messages)
					).get();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private void system(String... args) {
		ProcessBuilder processBuilder = new ProcessBuilder(args);
		try {
			Process start = processBuilder.start();
			int i = start.waitFor();
			System.out.println( String.format("result: {} => {}", Arrays.toString(args), i));
		} catch (InterruptedException e) {
			System.out.println( String.format("Interrupted", e));
			Thread.currentThread().interrupt();
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	//  private static DownloadedContent saveContent(String ext, MessageContentResponse response) {
	//	  System.out.println( String.format("Content-type: {}", response));
	//      DownloadedContent tempFile = createTempFile(ext);
	//      try (OutputStream outputStream = Files.newOutputStream(tempFile.path)) {
	//          ByteStreams.copy(response.getStream(), outputStream);
	//          log.info("Save {}: {}", ext, tempFile);
	//          return tempFile;
	//      } catch (IOException e) {
	//          throw new UncheckedIOException(e);
	//      }
	//  }

	//  private static DownloadedContent createTempFile(String ext) {
	//      String fileName = LocalDateTime.now() + "-" + UUID.randomUUID().toString() + "." + ext;
	//      Path tempFile = Application.downloadedContentDir.resolve(fileName);
	//      tempFile.toFile().deleteOnExit();
	//      return new DownloadedContent(tempFile, createUri("/downloaded/" + tempFile.getFileName()));
	//
	//  }

	private static String createUri(String path) {
		return ServletUriComponentsBuilder.fromCurrentContextPath()
				.path(path).toUriString();
	}

	public static class DownloadedContent {
		Path path;
		String uri;
	}
}
