package com.example;

import com.linecorp.bot.model.action.URIAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.util.function.Supplier;

import static java.util.Arrays.asList;

import java.net.URI;
import java.net.URISyntaxException;

public class ReceiptFlexMessageSupplier implements Supplier<FlexMessage> {
  
    public FlexMessage get() {
        final Separator separator = Separator.builder().margin(FlexMarginSize.XXL).build();
        final Box headerBlock = createBodyHeaderBox();
        final Box itemBlock = createBodyItemBlock();
        final Box summaryBlock = createBodySummaryBlock();
        Box footerBlock = null;
		try {
			footerBlock = createBodyFooterBlock();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        final Box bodyBlock = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(asList(
                        headerBlock,
                        separator,
                        itemBlock,
//                        separator,
                        summaryBlock,
                        separator,
                        footerBlock
                        ))
                .build();

        final Bubble bubble = Bubble.builder()
                .body(bodyBlock)
                .build();
        return new FlexMessage("ALT", bubble);
    }

    private Box createBodyHeaderBox() {
        final Text bodyHeaderText = Text.builder()
                .text("ข้อมูลของฉัน")
                .weight(Text.TextWeight.BOLD)
                .color("#1db446")
                .size(FlexFontSize.SM)
                .build();
        final Text bodyTitleHeaderText = Text.builder()
                .text("บัญชา ภุมรินทร์")
                .weight(Text.TextWeight.BOLD)
                .size(FlexFontSize.XXL)
                .margin(FlexMarginSize.MD)
                .build();
        final Text bodyTitleHeaderDetail = Text.builder()
                .text("ตำแหน่ง General Manager")
                .size(FlexFontSize.XS)
                .color("#aaaaaa")
                .wrap(true)
                .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(
                        bodyHeaderText,
                        bodyTitleHeaderText,
                        bodyTitleHeaderDetail)
                .build();
    }

    private Box createBodyItemBlock() {
        final Separator separator = Separator.builder().margin(FlexMarginSize.XXL).build();
        final Box item1 = createItem("Agent Code", "203123003");
        final Box item2 = createItem("Mobile", "098-256-7755");
        final Box item3 = createItem("LineId", "Buncha.P");
        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .margin(FlexMarginSize.XXL)
                .spacing(FlexMarginSize.SM)
                .contents(
                        item1,
                        item2,
                        item3)
                .build();
    }

    private Box createBodySummaryBlock() {
        final Separator separator = Separator.builder().margin(FlexMarginSize.XXL).build();
        final Box items = createSimpleItem("Address", FlexMarginSize.NONE);
        final Box total = createSimpleItem("246 Sukhumvit Rd, Khlong Toei, Bangkok 10110", FlexMarginSize.SM);
        final Box cash = createItem("License No.", "123241112");
        final Box change = createItem("License Date", "1 มกราคม 2566");
        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .margin(FlexMarginSize.XXL)
                .spacing(FlexMarginSize.SM)
                .contents(
                        items,
                        total,
                        cash,
                        change)
                .build();
    }

    private Box createBodyFooterBlock() throws URISyntaxException {
//        return Box.builder()
//                .layout(FlexLayout.HORIZONTAL)
//                .margin(FlexMarginSize.MD)
//                .contents(asList(
//                        Text.builder()
//                                .text("MEMO")
//                                .size(FlexFontSize.XS)
//                                .color("#aaaaaa")
//                                .flex(0)
//                                .build(),
//                        Text.builder()
//                                .text("-")
//                                .size(FlexFontSize.XS)
//                                .color("#aaaaaa")
//                                .align(FlexAlign.END)
//                                .build()
//                ))
//                .build();
    	return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(
                        Button.builder()
                                .action(new URIAction("ดูข้อมูลเพิ่มเติม", new URI("https://dd-uat.samsunglife.co.th"), new URIAction.AltUri(new URI("https://dd-uat.samsunglife.co.th"))))
                                .build()
                ).build();
    }

    private Box createItem(String name, String price) {
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(
                        Text.builder()
                                .text(name)
                                .size(FlexFontSize.SM)
                                .color("#555555")
                                .flex(0)
                                .build(),
                        Text.builder()
                                .text(price)
                                .size(FlexFontSize.SM)
                                .color("#111111")
                                .align(FlexAlign.END)
                                .build()
               ).build();
    }
    private Box createSimpleItem(String text, FlexMarginSize margin) {
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(
                        Text.builder()
                                .text(text)
                                .size(FlexFontSize.SM)
                                .color("#555555")
                                .flex(0)
                                .align(FlexAlign.START)
                                .margin(margin)
                                .build()
                ).build();
    }
    
}