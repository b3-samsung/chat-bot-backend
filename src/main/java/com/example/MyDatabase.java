package com.example;

import java.util.HashMap;
import java.util.Map;

public class MyDatabase {

	private static MyDatabase db;

	private Map<String, String> state;

	private MyDatabase() {
		state = new HashMap<String, String>();
	}

	public synchronized static final MyDatabase getInstance() {
		if (db == null) {
			db = new MyDatabase();
		}
		return MyDatabase.db;
	}

	public String getCurrentState(String id) {
		return getInstance().state.get(id);
	}

	public void setState(String id, String state) {
		getInstance().state.put(id, state);
	}
}
