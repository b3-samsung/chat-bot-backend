package com.example;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.UUID;

import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.SpelCompilerMode;
import org.springframework.expression.spel.SpelParserConfiguration;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.io.CharStreams;
import com.linecorp.bot.model.message.flex.container.FlexContainer;
import com.linecorp.bot.model.objectmapper.ModelObjectMapper;

public class Test {

	public static void main(String[] args) throws Exception {

		FlexContainer f = null;
			final ObjectMapper mapper = ModelObjectMapper.createNewObjectMapper()
					.configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true)
					.configure(Feature.ALLOW_COMMENTS, true)
					.configure(Feature.ALLOW_SINGLE_QUOTES, true)
					.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true)
					.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
					.configure(SerializationFeature.INDENT_OUTPUT, true);
			
			FileInputStream is = null;
			Reader reader = null;
			try {
				String body = null ;
				 is = new FileInputStream("src/main/resources/test.json");
				try {
					reader = new InputStreamReader(is);
					body = CharStreams.toString(reader);
		        }catch (Exception e) {
					// TODO: handle exception
				}
//				System.out.println(body);
				System.out.println(comp(body));
				 f = mapper.readValue(body, FlexContainer.class);
				 System.out.println(f);
				 
//				System.out.println(f.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				reader.close();
				is.close();
			}


//			System.out.println(comp(""));
		
	}
	
	
	public static String comp( final String text ) {
		X x = new X("X");
//		SpelParserConfiguration config = new SpelParserConfiguration(SpelCompilerMode.OFF,
//				Test.class.getClass().getClassLoader());
		ExpressionParser parser = new SpelExpressionParser();
		StandardEvaluationContext inventorContext = new StandardEvaluationContext();
		inventorContext.setVariable("x",x);

		// alternatively

		
	      // create spel expression parser instance

        // search the fileContent string and find spel expressions,
        // then evaluate the expressions

		StringBuffer sb = new StringBuffer(text);
        Integer start = 0, braketStart = 0;
        while ((braketStart = sb.indexOf("#{", start)) > -1) {
        	
            Integer braketClose = sb.indexOf("}", braketStart);
            
//            System.out.println(sb.length());
//            System.out.println(braketClose);
//            System.out.println(braketStart + 2 );

            
//            System.out.println(sb.charAt(braketStart ));
//            System.out.println(sb.charAt(braketClose));
            String expressionStr = sb.substring( Integer.valueOf(braketStart + 2), braketClose);
//            System.out.println(expressionStr);
            Expression expression = parser.parseExpression( "#"+expressionStr);
            String evaluatedValue = expression.getValue(inventorContext,  String.class);
//            System.out.println(evaluatedValue);
            sb.replace(braketStart, braketClose + 1, evaluatedValue);
            start = braketClose + evaluatedValue.length();
        }
        
        
		return sb.toString();
	}
	public static class X{
		
		private String x ;
		private Y y;
		
		public X(String x) {
			this.x = x;
			this.y = new Y( UUID.randomUUID().toString());
		}

		public String getX() {
			return x;
		}

		public void setX(String x) {
			this.x = x;
		}
		
		public Y getY() {
			return y;
		}

		public void setY(Y y) {
			this.y = y;
		}

		public static class Y{
			private String y;

			
			public Y(String y) {
				super();
				this.y = y;
			}

			public String getY() {
				return y;
			}

			public void setY(String y) {
				this.y = y;
			}
			
		}
		
	}
}
