/*
 * Copyright 2002-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.objectmapper.ModelObjectMapper;

import java.io.FileInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;


@SpringBootApplication(scanBasePackages = "com.example")
public class Main extends SpringBootServletInitializer implements CommandLineRunner{

	private static final Logger log = LoggerFactory.getLogger(Main.class);

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Main.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Main.class);
	}

	@Bean
	public ObjectMapper objectMapper() {
		final ObjectMapper mapper = ModelObjectMapper
				.createNewObjectMapper()
				.configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true)
				.configure(Feature.ALLOW_COMMENTS, true)
				.configure(Feature.ALLOW_SINGLE_QUOTES, true)
				.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true)
				.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true)
				.configure(SerializationFeature.INDENT_OUTPUT, true);
		return mapper;
	}

	@Autowired	
	private ObjectMapper mapper;
	
	public void run(String... args) throws Exception {
//		try(FileInputStream is = new FileInputStream("test.json")) {
//			FlexMessage f = mapper.readValue(is, FlexMessage.class);
//        }	catch (Exception e) {
//		e.printStackTrace();
//        	// TODO: handle exception
//		}
		
	}
}
